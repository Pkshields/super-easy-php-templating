# Super Easy PHP Templating
 
Templating for basic sites is too complicated for basic websites. This is a simple templating system to allow for reusing HTML templates without all the extra fluff.

Super Easy PHP Templating is licensed under the New BSD License.