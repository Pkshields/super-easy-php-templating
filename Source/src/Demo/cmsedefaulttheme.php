<?PHP
	include_once('../septmarkup.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"> 
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<title>CMSE Default Theme</title>
	<meta name="Keywords" content="cmse, cms exchange, cms, cme" /> 
	<meta name="Description" content="CME Exchange!" />

	<link rel="stylesheet" type="text/css" href="cmsedefaultstyle.css" />
</head>
<body>
	<div id="container">
		<?PHP SEPTMarkup::GetSnippet('cmsedefaulttheme-header.php'); ?>
		<div id="links">
			<p><a href="#">Links</a> | <a href="#">One</a> | <a href="#">Two</a> | <a href="#">Muzene</a> | <a href="#">Pkshields</a> | <a href="#">CME</a></p>
		</div>
		<div id="content">
			<h2><?PHP SEPTMarkup::GenerateBlock('title'); ?></h2>
			<p><?PHP SEPTMarkup::GenerateBlock('content'); ?></p>
		</div>
		<div id="footer">
			<p>Footer</p>
		</div>
	</div>
</body>
</html>