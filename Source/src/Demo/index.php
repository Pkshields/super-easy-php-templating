<?PHP
	include_once("../septtheme.php");

	$test = new SEPTTheme("cmsedefaulttheme.php");

	$test->StartBlock("title");
	echo "Demo Site!";
	$test->EndBlock();

	$test->StartBlock("content");

?>

<p>Demo theme provided by CMSE, a simple CMS also created by <a href="http://muzene.com">Muzene Studios</a>! For the record, this design is not considered good. Live demo!</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed tincidunt nisl, ut iaculis massa. Phasellus placerat tristique mi, et elementum metus tempor a. Praesent bibendum, sem non rhoncus pharetra, eros lacus molestie felis, sed scelerisque ligula lectus a massa. Donec purus nisi, ullamcorper in ante vitae, vestibulum eleifend nulla. Etiam arcu neque, dictum a laoreet a, sodales sed augue. Aenean porttitor lectus quis leo facilisis pulvinar. Nulla nec erat vel leo sagittis feugiat. Quisque volutpat diam nibh, in pharetra leo ullamcorper in. Integer in ultrices odio. Duis blandit lacus ac nunc aliquet, non tincidunt magna pharetra. Quisque pretium viverra justo a fermentum. Quisque varius volutpat felis sit amet fringilla. Duis posuere lectus eu neque varius lacinia. Etiam consequat augue id neque mollis ullamcorper. In urna turpis, mattis sed blandit eu, vulputate id turpis.</p>

<p>Fusce id pellentesque est. Suspendisse in ornare sem, vitae rhoncus ipsum. Nunc elementum turpis sit amet interdum tincidunt. Nullam euismod condimentum libero, vitae lacinia mauris. In luctus tortor elit, id fringilla sem eleifend quis. Integer vel pellentesque arcu. Sed cursus nulla eget turpis varius convallis. Etiam ipsum mi, tristique ut urna vitae, feugiat commodo sapien. In faucibus, est in elementum vulputate, erat tellus consectetur ante, at tempor neque est vel velit. Duis porta sem ac felis iaculis tristique. Fusce aliquam magna et lobortis rutrum. Sed a nulla massa. Vivamus lacus ligula, porttitor ac tempus non, mattis sed ligula.</p>


<?PHP

	$test->EndBlock();

	$test->RenderPage();
?>