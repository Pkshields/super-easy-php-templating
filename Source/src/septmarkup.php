<?PHP 
/**
 * SEPT Markup, the class that controls the markup within thge theme file itself
 *
 * @author Paul Shields - Pkshields.com
 */

	class SEPTMarkup
	{
		/**
		 * Page data from the user, taken from a specific SEPTTheme instance
		 *
		 * @access private
		 * @var Array[string] string
		 */
		private static $data = null;

		/**
		 * Set the location for a block to be inserted within the theme
		 *
		 * @param string blockName	Name of block to be inserted
		 */
		public static function GenerateBlock($blockName)
		{
			if (array_key_exists($blockName, self::$data))
				echo self::$data[$blockName];
		}

		/**
		 * Access a block directly compared to the HTML output of GenerateBlock
		 *
		 * @param string blockName	Name of block to be getted
		 */
		public static function GetBlock($blockName)
		{
			if (array_key_exists($blockName, self::$data))
				return self::$data[$blockName];
		}

		/**
		 * Provide the data to start rendering the page with
		 *
		 * @param Array[string] string data		Data to render the page with
		 */
		public static function StartRender($data)
		{
			if (is_null(self::$data))
				self::$data = $data;
			else
				throw new BadMethodCallException("Page already being rendered, cannot render another page.");
		}

		/**
		 * Get a snippet to render at this current spot
		 *
		 * @param string snippetFile		File containing snippet to render
		 */
		public static function GetSnippet($snippetFile)
		{
			include($snippetFile);
		}

		/**
		 * Complete the page render by clearing the data, ready for another render
		 */
		public static function CompleteRender()
		{
			self::$data = null;
		}
	}
?>